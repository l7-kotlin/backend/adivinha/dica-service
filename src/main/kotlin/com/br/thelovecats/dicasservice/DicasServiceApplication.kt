package com.br.thelovecats.dicasservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DicasServiceApplication

fun main(args: Array<String>) {
	runApplication<DicasServiceApplication>(*args)
}
